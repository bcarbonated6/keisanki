using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Begin : MonoBehaviour
{
    private GameObject mainUIbase;
    private GameObject MainUI;
    private GameObject Startbutton;
    private GameObject Level;
    private GameObject Buttongroup;
    private RectTransform LevelRect;
    private Vector2 vec2;
    private const float Y = 75;
    private const float X = 260;
    private bool Move;
    // Start is called before the first frame update
    void Start()
    {
        vec2 = new Vector2 (X, Y);
        mainUIbase = GameObject.Find("MainUIbase");
        MainUI = mainUIbase.transform.Find("MainUI").gameObject;
        Buttongroup = MainUI.transform.Find("ButtonGroup").gameObject;
        Level = Buttongroup.transform.Find("LevelGroup").gameObject;
        LevelRect = Level.GetComponent<RectTransform>();
        Startbutton = Buttongroup.transform.Find("StartButton").gameObject;
        Move = false;
    }
    // Update is called once per frame
    void Update()
    {
        if (Move == true && LevelRect.anchoredPosition.x >= X)
        { 
            LevelRect.anchoredPosition = LevelRect.anchoredPosition - new Vector2(4, 0);             
        }
        
    }
    public void OnClick()
    {
        Move = true;     
    }
}