using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Slide : MonoBehaviour
{
    private GameObject mapbase;
    private GameObject canvas;
    private GameObject MapBox;
    private GameObject slider;
    private RectTransform MapBoxRectTransform;
    private Slider S1;
    private Vector2 position;
    private float slidervalue;
    // Start is called before the first frame update
    void Start()
    {
        mapbase = GameObject.Find("Mapbase");
        canvas = mapbase.transform.Find("Canvas").gameObject;
        MapBox = canvas.transform.Find("GameObject").gameObject;
        slider = canvas.transform.Find("Slider1").gameObject;
        MapBoxRectTransform = MapBox.GetComponent<RectTransform>();
        position = MapBoxRectTransform.anchoredPosition;
        S1 = slider.GetComponent<Slider>();
    }
    // Update is called once per frame
    void Update()
    {
        slidervalue = S1.value;
        MapBoxRectTransform.anchoredPosition = position + new Vector2(0 , slidervalue);
    }
}
