using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class Exit : MonoBehaviour
{
   // private GameObject MainUI;
    //private GameObject Buttongroup;
    //private GameObject ExitButton;
    // Start is called before the first frame update
    void Start()
    {
      //  MainUI = GameObject.Find("MainUI");
      //  Buttongroup = MainUI.transform.Find("ButtonGroup").gameObject;
      //  ExitButton = Buttongroup.transform.Find("ExitButton").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnClick()
    {
          #if UNITY_EDITOR
              UnityEditor.EditorApplication.isPlaying = false;
          #else
            Application.Quit();
          #endif
    }
}
