using UnityEngine;
using UnityEngine.EventSystems;
public class Slot : MonoBehaviour, IDropHandler
{
    private GameObject mapbase;
    GameObject can;
    GameObject map;
    private void Start()
    {
        mapbase = GameObject.Find("Mapbase");
        can = mapbase.transform.Find("Canvas").gameObject;
        map = can.transform.Find("GameObject_map").gameObject;
    }
    public void OnDrop(PointerEventData eventData)
    {
        eventData.pointerDrag.transform.parent = map.transform;
        eventData.pointerDrag.GetComponent<RectTransform>().anchoredPosition = GetComponent<RectTransform>().anchoredPosition;
        Debug.Log("aaaaa");
    }

 
}
