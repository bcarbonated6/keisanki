using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;

public class ReturnToUI : MonoBehaviour
{


    private GameObject canvas;
    private GameObject mapbase;
    private GameObject MainUI;
    private GameObject mainUIbase;
    // Start is called before the first frame update
    void Start()
    {
        mainUIbase = GameObject.Find("MainUIbase");
        MainUI = mainUIbase.transform.Find("MainUI").gameObject;
        mapbase = GameObject.Find("Mapbase");
        canvas = mapbase.transform.Find("Canvas").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public void OnClick()
    {
        canvas.SetActive(false);
        if (canvas.active == false)
        {
            MainUI.SetActive(true);
        }

    }

 }
