using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class Drag : MonoBehaviour, IDragHandler, IBeginDragHandler, IEndDragHandler
{
    private CanvasGroup canvasGroup;
    private RectTransform recTrans;
    private Vector2 vector2;
    private string Currenttag;
    private Vector2 size;
    private GameObject mapbase;
    GameObject can;
    GameObject TargetMap;
    GameObject AllMap;
    GameObject NowParent;
    private void Start()
    {
       
        canvasGroup = GetComponent<CanvasGroup>();
        recTrans = GetComponent<RectTransform>();
        vector2 = recTrans.anchoredPosition;
        Currenttag = transform.tag;
        size = recTrans.sizeDelta;
        Debug.Log("aaawwww");
        mapbase = GameObject.Find("Mapbase");
        can = mapbase.transform.Find("Canvas").gameObject;
        AllMap = can.transform.Find("GameObject_map").gameObject;
        NowParent = transform.parent.gameObject;

    }


    public void OnBeginDrag(PointerEventData eventData)
    {
        

        TargetMap = AllMap.transform.Find(Currenttag).gameObject;
        transform.parent = AllMap.transform;
        canvasGroup.blocksRaycasts = false; 
        recTrans.sizeDelta = TargetMap.GetComponent<RectTransform>().sizeDelta;
        Debug.Log("aaww");
        
    }
    public void OnDrag(PointerEventData eventData)
    {

        recTrans.anchoredPosition += eventData.delta;
        //transform.position = Input.mousePosition;                
        //Debug.Log("aaaqqqq");

    }

    public void OnEndDrag(PointerEventData eventData)
    {
        canvasGroup.blocksRaycasts=true;
        if (eventData.pointerEnter == null || eventData.pointerEnter.tag != Currenttag)
        {
            Debug.Log("ssss");
            recTrans.sizeDelta = size;
            transform.parent = NowParent.transform;
            recTrans.anchoredPosition = vector2;
            

        }
        Debug.Log("aaarrrr");
    }
}
